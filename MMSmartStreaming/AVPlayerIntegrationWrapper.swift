import Foundation
import CoreTelephony
import UIKit
import AVFoundation
import os

import MMSmartStreamingPrivate
import AdManager

private var AVFoundationPlayerViewControllerKVOContext = 0
private var AVFoundationPlayerViewControllerKVOContextPlayer = 0

public class MMAVAssetInformation{
    /*
     * Creates an object to hold information identifying the asset that is to be played back on the AVPlayer
     * User must specify URL of the asset.
     * User may optionally specify the identifier identifying the asset, its name and collection to which this asset belongs
     */
    public init(assetURL aURL:String, assetID aId:String?, assetName aName:String?, videoId vId:String?){
        assetURL = aURL
        
        if let aId = aId{
            assetID = aId
        }
        
        if let aName = aName{
            assetName = aName
        }
        
        if let vId = vId{
            videoId = vId
        }
        
        customKVPs = [:]
    }
    
    /*
     * Lets user specify the custom metadata to be associated with the asset, for example - Genre, DRM etc
     *
     * Call to this API is optional
     */
    public func addCustomKVP(_ key:String, _ value:String){
        customKVPs[key] = value
    }
    
    /*
     * Sets the mode to be used for QBR and the meta file url from where content metadata can be had.
     * Meta file URL is to be provided only if metadata cant be had on the default location.
     *
     * Please note that call to this method is needed only if QBR is integrated to the player.
     */
    public func setQBRMode(_ mode:MMQBRMode, withMetaURL metaURL:URL?){ //Needed only when QBR is to be integrated
        qbrMode = mode
        if let metaURL = metaURL{
            metafileURL = metaURL
        }
    }
    
    public var assetURL:String //URL of the Asset
    public var assetID:String? //optional identifier of the asset
    public var assetName:String? //optional name of the asset
    
    public var videoId:String? //optional identifier of the asset group (or) sub asset
    
    public var qbrMode:MMQBRMode = MMQBRMode.QBRModeDisabled //Needed only when QBR is to be integrated
    public var metafileURL:URL? //Needed only when QBR is to be integrated
    
    public var customKVPs:[String:String] //Custom Metadata of the asset
}

public enum MMOverridableMetrics{
    /**
     * Latency - Time between when user requests the start of the playback session and playback starts.
     */
    case Latency,
    
    /**
     * CDN - IP address of manifest server
     */
    CDN,
    
    /**
     * DurationWatched - Duration of time that the player was in the PLAYING state, excluding advertisement play time.
     */
    DurationWatched
}

public class MMRegistrationInformation{
    let kEXTERNALSUBSCRIBERIDADDEDDATE = "EXTERNAL-SUBSCRIBER-ID-ADDEDDATE"
    let kEXTERNALSUBSCRIBERID = "EXTERNAL-SUBSCRIBER-ID"
    /*
     * Creates the object to have information identifying the customer, subscriber, and player to which integration is done
     */
    public init(customerID cID:String, playerName pName:String){
        customerID = cID
        component = "IOSSDK"
        playerName = pName
    }

    /*
     * Some business organizations may would like to do analytics segmented by group.
     * For example, a Media House may have many divisions, and will like to categorize their analysis
     * based on division. Or a content owner has distributed content to various resellers and would
     * like to know the reseller from whom the user is playing the content.
     * In this case every reseller will have separate application, and will configure the domain name.
     *
     * Call to this API is optional
     */
    public func setDomain(_ dName:String?){
        if let dName = dName{
            domainName = dName
        }
    }
    
    /*
     * Provides the subscriber information to the SDK.
     * Subscriber information includes identifier identifying the subscriber (genrally email id, or UUID of app installation etc.),
     * Its type - For example Premium, Basic etc (Integrators can choose any value for type depending on the damain of business in
     * which player is used. From perspective of Smartsight, it is opaque data, and is not interpreted in any way by it.
     * Tag - Additional metadata corresponding to the asset. From perspective of Smartsight, no meaning is attached to it, and it is
     * reflect as is.
     *
     * Call to this API is optional
     */
    public func setSubscriberInformation(subscriberID subsID:String?, subscriberType subsType:String?, subscriberTag subsTag:String?){
        var isUUIDNeeded = false
        if subsID == nil {
            isUUIDNeeded = true
        } else if subsID!.isEmpty {
            isUUIDNeeded = true
        }
        
        if let subsID = subsID{
            subscriberID = subsID
        }
        if isUUIDNeeded {
            if UserDefaults.standard.value(forKey: kEXTERNALSUBSCRIBERIDADDEDDATE) != nil {
                let externalSubscriberIDAddedDate = UserDefaults.standard.value(forKey: kEXTERNALSUBSCRIBERIDADDEDDATE) as! Date
                let today = Date()
                let dayDifference = today.interval(ofComponent: .day, fromDate: externalSubscriberIDAddedDate)
                let lastSetExternalSubscriberID = UserDefaults.standard.value(forKey: kEXTERNALSUBSCRIBERID) as? String
                
                if (dayDifference <= 30 && lastSetExternalSubscriberID != nil) {
                    subscriberID = lastSetExternalSubscriberID
                } else {
                    subscriberID = UUID().uuidString
                    UserDefaults.standard.set(subscriberID, forKey: kEXTERNALSUBSCRIBERID)
                    UserDefaults.standard.set(Date(), forKey: kEXTERNALSUBSCRIBERIDADDEDDATE)
                    UserDefaults.standard.synchronize()
                }
            } else {
                subscriberID = UUID().uuidString
                UserDefaults.standard.set(subscriberID, forKey: kEXTERNALSUBSCRIBERID)
                UserDefaults.standard.set(Date(), forKey: kEXTERNALSUBSCRIBERIDADDEDDATE)
                UserDefaults.standard.synchronize()
            }
        }
        if let subsType = subsType{
            subscriberType = subsType
        }
        if let subsTag = subsTag{
            subscriberTag = subsTag
        }
    }
    
    /**
     * Sets the player information. Please note that brand, model and version mentioned here are with respect to player and not wrt device
     * i.e. Even though brand for device is Apple, but brand here could be the brand, that integrator want to assign to this player.
     * For example - It could be the name of Media Vendor.
     * Model - This could be used to further classify the player, for example XYZ framework based player, or VOD player or Live player etc
     * Version - This is used to indicate the version of the player
     * All these params are optionals and you may set them to nil
     *
     * Call to this API is optional
     */
    public func setPlayerInformation(brand:String?, model:String?, version:String?){
        if let brand = brand{
            playerBrand = brand
        }
        
        if let model = model{
            playerModel = model
        }
        
        if let version = version{
            playerVersion = version
        }
    }
    
    public var customerID:String
    public var component:String
    
    public var playerName:String
    public var domainName:String?
    
    public var subscriberID:String?
    public var subscriberType:String?
    public var subscriberTag:String?
    
    
    public var playerBrand:String?
    public var playerModel:String?
    public var playerVersion:String?
    
}

class ReachabilityManager: NSObject {
    public static  let shared = ReachabilityManager()
    let reachability = ReachabilityMM()!
    var reachabilityStatus: ReachabilityMM.NetworkStatus = .notReachable
    
    @objc func reachabilityChanged(notification: Notification) {
        let reachability = notification.object as! ReachabilityMM
        var connInfo:MMConnectionInfo!
        
        func getDetailedMobileNetworkType() {
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let arrayNetworks = phoneInfo.serviceCurrentRadioAccessTechnology {
                    var networkString = ""
                    for value in arrayNetworks.values {
                        networkString = value
                    }
                    if networkString == CTRadioAccessTechnologyLTE{
                        connInfo = .cellular_4G
                    }else if networkString == CTRadioAccessTechnologyWCDMA{
                        connInfo = .cellular_3G
                    }else if networkString == CTRadioAccessTechnologyEdge{
                        connInfo = .cellular_2G
                    }
                }
            } else {
                let networkString = phoneInfo.currentRadioAccessTechnology
                if networkString == CTRadioAccessTechnologyLTE{
                    connInfo = .cellular_4G
                }else if networkString == CTRadioAccessTechnologyWCDMA{
                    connInfo = .cellular_3G
                }else if networkString == CTRadioAccessTechnologyEdge{
                    connInfo = .cellular_2G
                }
            }
        }
        
        switch reachability.currentReachabilityStatus {
        case .notReachable:
            connInfo = .notReachable
        case .reachableViaWiFi:
            connInfo = .wifi
        case .reachableViaWWAN:
            connInfo = .cellular
            getDetailedMobileNetworkType()
        }
        
        if (connInfo != nil) {
            AVPlayerIntegrationWrapper.shared.reportNetworkType(connInfo:connInfo)
        }
    }
    
    func startMonitoring() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.reachabilityChanged),
                                               name: ReachabilityChangedNotificationMM,
                                               object: reachability)
        do{
            try reachability.startNotifier()
        } catch {
        }
    }
    
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: ReachabilityChangedNotificationMM,
                                                  object: reachability)
    }
}

public class AVPlayerIntegrationWrapper:NSObject, MMSmartStreamingObserver{
    /*
     * Singleton instance of the adaptor
     */
    public static  let shared = AVPlayerIntegrationWrapper()
    
    /**
     * Gets the version of the SDK
     */
    public static func getVersion()->String!{
        return "3.7.5/\(String(describing: MMSmartStreaming.getVersion()!))"
    }
    
    /**
     * If for some reasons, accessing the content manifest by SDK interferes with the playback. Then user can disable the manifest fetch by the SDK.
     * For example - If there is some token scheme in content URL, that makes content to be accessed only once, then user of SDK may will like to call this API.
     * So that player can fetch the manifest
     */
    public static func disableManifestsFetch(disable:Bool){
        return MMSmartStreaming.disableManifestsFetch(disable)
    }
    
    /**
     * Allows user of SDK to provide information on Customer, Subscriber and Player to the SDK
     * Please note that it is sufficient to call this API only once for the lifetime of the application, and all playback sessions will reuse this information.
     *
     * Note - User may opt to call initializeAssetForPlayer instead of calling this API, and provide the registration information in its param every time as they provide the asset info. This might help ease the integration.
     *
     * This API doesnt involve any network IO, and is very light weight. So calling it multiple times is not expensive
     */
    public static func setPlayerRegistrationInformation(registrationInformation pInfo:MMRegistrationInformation?, player aPlayer:AVPlayer?){
        if let pInfo = pInfo{
            AVPlayerIntegrationWrapper.logDebugStatement("=============setPlayerInformation - pInfo=============")
            AVPlayerIntegrationWrapper.registerMMSmartStreaming(playerName: pInfo.playerName, custID: pInfo.customerID, component: pInfo.component, subscriberID: pInfo.subscriberID, domainName: pInfo.domainName, subscriberType: pInfo.subscriberType, subscriberTag: pInfo.subscriberTag)
            AVPlayerIntegrationWrapper.reportPlayerInfo(brand: pInfo.playerBrand, model: pInfo.playerModel, version: pInfo.playerVersion)
        }
        if let oldPlayer = AVPlayerIntegrationWrapper.shared.player{
            AVPlayerIntegrationWrapper.shared.cleanupSession(player: oldPlayer)
        }
        if let newPlayer = aPlayer{
            AVPlayerIntegrationWrapper.shared.createSession(player: newPlayer)
        }
    }
    
    /**
     * Application may create the player with the AVAsset for every session they do the playback
     * User of API must provide the asset Information
     */
    public static func initializeAssetForPlayer(assetInfo aInfo:MMAVAssetInformation, registrationInformation pInfo:MMRegistrationInformation?, player aPlayer:AVPlayer?){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        AVPlayerIntegrationWrapper.logDebugStatement("================initializeAssetForPlayer \(aInfo.assetURL)=============")
        AVPlayerIntegrationWrapper.setPlayerRegistrationInformation(registrationInformation: pInfo, player:aPlayer)
        AVPlayerIntegrationWrapper.changeAssetForPlayer(assetInfo: aInfo, player: aPlayer)
    }
    
    /**
     * Whenever the asset with the player is changed, user of the API may call this API
     * Please note either changeAssetForPlayer or initializeAssetForPlayer should be called
     */
    public static func changeAssetForPlayer(assetInfo aInfo:MMAVAssetInformation, player aPlayer:AVPlayer?){
        AVPlayerIntegrationWrapper.logDebugStatement("================changeAssetForPlayer \(aInfo.assetURL)=============")
        AVPlayerIntegrationWrapper.shared.assetInfo = aInfo
        AVPlayerIntegrationWrapper.shared.cleanupCurrItem();
        
        if let newPlayer = aPlayer{
            if let oldPlayer = AVPlayerIntegrationWrapper.shared.player{
                if oldPlayer != newPlayer{
                    AVPlayerIntegrationWrapper.shared.cleanupSession(player: oldPlayer)
                    AVPlayerIntegrationWrapper.shared.player = nil;
                    AVPlayerIntegrationWrapper.shared.createSession(player: newPlayer)
                }
                
                if let playerItem = newPlayer.currentItem{
                    AVPlayerIntegrationWrapper.shared.initSession(player: newPlayer, playerItem: playerItem , deep: true)
                }
            }
        }
    }
    
    /**
     * Once the player is done with the playback session, then application should call this API to clean up observors set with the player and the player's current item
     */
    public static func cleanUp(){
        AVPlayerIntegrationWrapper.logDebugStatement("================cleanUp=============")
        shared.cleanupInstance()
    }
    
    /**
     * Application may update the subscriber information once it is set via MMRegistrationInformation
     */
    public static func updateSubscriber(subscriberId:String!, subscriberType:String!, subscriberMetadata:String!){
        MMSmartStreaming.updateSubscriber(withID: subscriberId, andType: subscriberType, withTag:subscriberMetadata)
    }
    
    /**
     * Application may report the custom metadata associated with the content using this API.
     * Application can set it as a part of MMAVAssetInformation before the start of playback, and
     * can use this API to set metadata during the course of the playback.
     */
    public func reportCustomMetadata(key:String!, value:String!){
        mmSmartStreaming.reportCustomMetadata(withKey: key, andValue: value)
    }
    
    /**
     * Used for debugging purposes, to enable the log trace
     */
    public func enableLogTrace(logStTrace:Bool){
        AVPlayerIntegrationWrapper.enableLogging = logStTrace
        mmSmartStreaming.enableLogTrace(logStTrace)
    }
    
    /**
     * If application wants to send application specific error information to SDK, the application can use this API.
     * Note - SDK internally takes care of notifying the error messages provided to it by AVFoundation framwork
     */
    public func reportError(error:String, playbackPosMilliSec:Int){
        mmSmartStreaming.reportError(error, atPosition: playbackPosMilliSec)
    }
    
    public static func reportMetricValue(metricToOverride:MMOverridableMetrics, value:String!){
        switch metricToOverride {
        case MMOverridableMetrics.CDN:
            AVPlayerIntegrationWrapper.shared.mmSmartStreaming.reportMetricValue(for: MMOverridableMetric.ServerAddress, value: value)
        default:
            print("Only CDN metric is overridable as of now ...")
        }
    }
    
    private static func logDebugStatement(_ logStatement:String){
        if(AVPlayerIntegrationWrapper.enableLogging){
            //os_log("mediamelon.smartstreaming.avplayer-ios %{public}@", logStatement)
            NSLog("%s", logStatement)
        }
    }
    
    private static func registerMMSmartStreaming(playerName:String, custID:String, component:String,  subscriberID:String?, domainName:String?, subscriberType:String?, subscriberTag:String?){
        MMSmartStreaming.registerForPlayer(withName: playerName, forCustID: custID, component: component, subsID: subscriberID, domainName: domainName, andSubscriberType: subscriberType, withTag:subscriberTag);
        let phoneInfo = CTTelephonyNetworkInfo()
        var operatorName = ""
        if let carrierName = phoneInfo.subscriberCellularProvider?.carrierName{
            operatorName = carrierName
        }

        let brand = "Apple"
        let model = UIDevice.current.model
        
        let osName = "iOS"
        let osVersion =  UIDevice.current.systemVersion
        
        let screenWidth = Int(UIScreen.main.bounds.width)
        let screenHeight = Int(UIScreen.main.bounds.height)
        
        MMSmartStreaming.reportDeviceInfo(withBrandName: brand, deviceModel: model, osName: osName, osVersion: osVersion, telOperator: operatorName, screenWidth: screenWidth, screenHeight: screenHeight, andType: model)
    }

    private static func reportPlayerInfo(brand:String?, model:String?, version:String?){
        MMSmartStreaming.reportPlayerInfo(withBrandName: brand, model: model, andVersion: version)
    }
    
    private func initializeSession(mode:MMQBRMode!, manifestURL:String!, metaURL:String?, assetID:String?, assetName:String?, videoId:String?){
        self.contentURL = manifestURL
        var connectionInfo:MMConnectionInfo!
        let reachability = ReachabilityMM()
        
        func getDetailedMobileNetworkType() {
            let phoneInfo = CTTelephonyNetworkInfo()
            if #available(iOS 12.0, *) {
                if let arrayNetworks = phoneInfo.serviceCurrentRadioAccessTechnology {
                    var networkString = ""
                    for value in arrayNetworks.values {
                        networkString = value
                    }
                    if networkString == CTRadioAccessTechnologyLTE{
                        connectionInfo = .cellular_4G
                    }else if networkString == CTRadioAccessTechnologyWCDMA{
                        connectionInfo = .cellular_3G
                    }else if networkString == CTRadioAccessTechnologyEdge{
                        connectionInfo = .cellular_2G
                    }
                }
            } else {
                let networkString = phoneInfo.currentRadioAccessTechnology
                if networkString == CTRadioAccessTechnologyLTE{
                    connectionInfo = .cellular_4G
                }else if networkString == CTRadioAccessTechnologyWCDMA{
                    connectionInfo = .cellular_3G
                }else if networkString == CTRadioAccessTechnologyEdge{
                    connectionInfo = .cellular_2G
                }
            }
        }
        
        if let reachability = reachability{
            do{
                try reachability.startNotifier()
                let status = reachability.currentReachabilityStatus
                
                if(status == .notReachable){
                    connectionInfo = .notReachable
                }
                else if (status == .reachableViaWiFi){
                    connectionInfo = .wifi
                }
                else if (status == .reachableViaWWAN){
                    connectionInfo = .cellular
                    getDetailedMobileNetworkType()
                }
            }
            catch{
                
            }
        }
        if (connectionInfo != nil) {
            mmSmartStreaming.reportNetworkType(connectionInfo)
        }
        mmSmartStreaming.initializeSession(with: mode, forManifest: manifestURL, metaURL: metaURL, assetID: assetID, assetName: assetName, videoID:videoId, for: self)
    }
    
    func reportNetworkType(connInfo:MMConnectionInfo){
        mmSmartStreaming.reportNetworkType(connInfo)
    }

    private func reportLocation(latitude:Double, longitude:Double){
        mmSmartStreaming.reportLocation(withLatitude: latitude, andLongitude: longitude)
    }
    
    public func sessionInitializationCompleted(with status: MMSmartStreamingInitializationStatus, andDescription description: String?, forCmdWithId cmdId: Int) {
        AVPlayerIntegrationWrapper.logDebugStatement("sessionInitializationCompleted - status \(status) description \(String(describing: description))")
    }
    
    private func reportChunkRequest(chunkInfo: MMChunkInformation!){
        mmSmartStreaming.reportChunkRequest(chunkInfo)
    }
    
    private func setPresentationInformation(presentationInfo:MMPresentationInfo!){
        mmSmartStreaming.setPresentationInformation(presentationInfo)
    }
    
    private func reportDownloadRate(downloadRate: Int!){
        mmSmartStreaming.reportDownloadRate(downloadRate)
    }
    
    private func reportBufferingStarted(){
        mmSmartStreaming.reportBufferingStarted()
    }
    
    private func reportBufferingCompleted(){
        mmSmartStreaming.reportBufferingCompleted()
    }
    
    private func reportABRSwitch(prevBitrate:Int, newBitrate:Int){
        mmSmartStreaming.reportABRSwitch(fromBitrate: prevBitrate, toBitrate: newBitrate)
    }
    
    private func reportFrameLoss(lossCnt:Int){
        mmSmartStreaming.reportFrameLoss(lossCnt)
    }
    
    @objc private func timeoutOccurred(){
        guard self.player != nil else {
            return
        }
        
        mmSmartStreaming.reportPlaybackPosition(getPlaybackPosition())
    }
    
    private func logIntegration(str:String){
        
    }
    
    private func reportPresentationSize(width:Int, height:Int){
        mmSmartStreaming.reportPresentationSize(withWidth: width, andHeight: height)
    }
    
    private func getPlaybackPosition()->Int{
        guard let player = player else{
            return 0;
        }

        let time = player.currentTime();
        if(time.timescale > 0){
            return Int((time.value)/(Int64)(time.timescale)) * 1000;
        }else{ //Avoid dividing by 0 , -ve should not be expected
            return 0;
        }
    }
    
    private func cleanupInstance(){
        cleanupCurrItem()
        cleanupSession(player: self.player)
    }
    
    private func cleanupCurrItem(){
        guard let player = self.player else{
            return;
        }
        
        guard let playerItem = player.currentItem else {
            return;
        }
        
        resetSession(item: playerItem)
    }
    
    private func playbackDidReachEnd(notification noti:Notification){
        AVPlayerIntegrationWrapper.logDebugStatement("--- playbackDidReachEnd ---")
        cleanupCurrItem();
    }
    
    private func playbackFailedToPlayTillEnd(notification noti:Notification){
        AVPlayerIntegrationWrapper.logDebugStatement("--- playbackFailedToPlayTillEnd ---")
        mmSmartStreaming.reportError(noti.description, atPosition: -1)
        currentState = .ERROR
        cleanupCurrItem();
    }
    
    private func handleErrorWithMessage(message: String?, error: Error? = nil) {
        AVPlayerIntegrationWrapper.logDebugStatement("--- Error occurred with message: \(String(describing: message)), error: \(String(describing: error)).")
        mmSmartStreaming.reportError(String(describing: error), atPosition: getPlaybackPosition())
    }
    
    private func reset(){
        lastPlaybackPos = 0
        lastPlaybackPosRecordTime = 0
        sessionInStartedState = false
        presentationInfoSet = false
        infoEventIdx = 0
        errEventIdx = 0
        infoEventsIdxToSkip = -1
        errorEventsIdxToSkip = -1
        lastObservedBitrateOfContinuedSession = -1.0
        lastObservedDownlaodRateOfContinuedSession = -1
        durationWatchedTotal = 0
        currentBitrate = 0
        frameLossCnt = 0
        isInitialBitrateReported = false
        contentURL = nil
        currentState = CurrentPlayerState.IDLE
        loadEventTriggered = false
        
        if AVPlayerIntegrationWrapper.appNotificationObsRegistered == false{
            AVPlayerIntegrationWrapper.appNotificationObsRegistered = true
            
            NotificationCenter.default.addObserver(
                forName: NSNotification.Name.UIApplicationWillResignActive,
                object: nil,
                queue: nil) { (notification) in
                    ReachabilityManager.shared.stopMonitoring()
            }
            
            NotificationCenter.default.addObserver(
                forName: NSNotification.Name.UIApplicationDidBecomeActive,
                object: nil,
                queue: nil) { (notification) in
                    ReachabilityManager.shared.startMonitoring()
            }
            ReachabilityManager.shared.startMonitoring()
        }
    }
    
    private func createSession(player:AVPlayer){
        AVPlayerIntegrationWrapper.logDebugStatement("*** createSession")
        self.player = player
        self.addPlayerObservors();
        timer = nil
    }
    
    private func initSession(player:AVPlayer, playerItem:AVPlayerItem, deep:Bool){
        AVPlayerIntegrationWrapper.logDebugStatement("*** initSession")
        var infoEventsIdxToSkipCache = -1
        var errorEventsIdxToSkipCache = -1
        
        if (infoEventsIdxToSkip != -1){
            infoEventsIdxToSkipCache = infoEventsIdxToSkip
        }
        if (errorEventsIdxToSkip != -1){
            errorEventsIdxToSkipCache = errorEventsIdxToSkip
        }
        
        if(deep){
            reset()
        }
        
        infoEventsIdxToSkip = infoEventsIdxToSkipCache
        errorEventsIdxToSkip = errorEventsIdxToSkipCache
        
        guard let assetInfo = self.assetInfo else{
            AVPlayerIntegrationWrapper.logDebugStatement("!!! Error - assetInfo not set !!!")
            return
        }
        
        self.sessTerminated = false;
        
        if(deep){
            AVPlayerIntegrationWrapper.shared.initializeSession(mode: assetInfo.qbrMode, manifestURL: assetInfo.assetURL, metaURL: assetInfo.metafileURL? .absoluteString, assetID: assetInfo.assetID, assetName: assetInfo.assetName, videoId: assetInfo.videoId)
            for (key, value) in assetInfo.customKVPs{
                AVPlayerIntegrationWrapper.shared.reportCustomMetadata(key: key, value: value)
            }
            AVPlayerIntegrationWrapper.shared.reportUserInitiatedPlayback();
        }else{
            for (key, value) in assetInfo.customKVPs{
                AVPlayerIntegrationWrapper.shared.reportCustomMetadata(key: key, value: value)
            }
        }
        
        self.timer = Timer.scheduledTimer(timeInterval: self.TIME_INCREMENT, target:self, selector:#selector(AVPlayerIntegrationWrapper.timeoutOccurred), userInfo: nil, repeats: true)
        
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.Duration.rawValue, options: [.new], context: &AVFoundationPlayerViewControllerKVOContext)
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.Playable.rawValue, options: [.new], context: &AVFoundationPlayerViewControllerKVOContext)
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.ItemStatus.rawValue, options: [.new, .initial], context: &AVFoundationPlayerViewControllerKVOContext)
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PresentationSize.rawValue, options: [.new, .initial], context: &AVFoundationPlayerViewControllerKVOContext)
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PlaybackBufferEmpty.rawValue, options: [.new, .initial], context: &AVFoundationPlayerViewControllerKVOContext)
        playerItem.addObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PlaybackLikelyToKeepUp.rawValue, options: [.new, .initial], context: &AVFoundationPlayerViewControllerKVOContext)
        
        var observor = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem, queue: nil, using: {  (not) in  self.playbackDidReachEnd(notification:not)})
        notificationObservors.append(observor)
        observor = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemFailedToPlayToEndTime, object: playerItem, queue: nil, using: {  (not) in  self.playbackFailedToPlayTillEnd(notification:not)})
        notificationObservors.append(observor)
        observor = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemNewAccessLogEntry, object: playerItem, queue: nil, using: {  (not) in  self.newAccessLogEntryRecvd(notification:not)})
        notificationObservors.append(observor)
        observor = NotificationCenter.default.addObserver(forName: NSNotification.Name.AVPlayerItemNewErrorLogEntry, object: playerItem, queue: nil, using: {  (not) in  self.newErrorLogEntryRecvd(notification:not)})
        notificationObservors.append(observor)
        
        self.playerItemObserverAdded = playerItem
        if (infoEventsIdxToSkip >= 0 || errorEventsIdxToSkip >= 0){
            processEventsRegister()
            processErrorEventsRegister()
            infoEventsIdxToSkip = -1
            errorEventsIdxToSkip = -1
        }
        
        let url = (playerItem.asset as? AVURLAsset)?.url
        AVPlayerIntegrationWrapper.logDebugStatement("Initializing for \(String(describing: url))")
    }
    
    private func resetSession(item:AVPlayerItem?){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        guard let playerItem = item else{
            return;
        }
        
        guard let observerAddedPlayerItem = self.playerItemObserverAdded else {
            return
        }
        
        if (observerAddedPlayerItem == item){
            for item in self.notificationObservors {
                NotificationCenter.default.removeObserver(item);
            }
            self.playerItemObserverAdded = nil
            self.notificationObservors.removeAll();

            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.Duration.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);
            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.Playable.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);
            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.ItemStatus.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);
            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PresentationSize.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);
            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PlaybackBufferEmpty.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);
            playerItem.removeObserver(self, forKeyPath: AVPlayerItemPropertiesToObserve.PlaybackLikelyToKeepUp.rawValue, context: &AVFoundationPlayerViewControllerKVOContext);

            if let tmr = self.timer{
                tmr.invalidate();
            }
            
            self.reportStoppedState()
            self.sessTerminated = true;
            let url = (playerItem.asset as? AVURLAsset)?.url
            AVPlayerIntegrationWrapper.logDebugStatement("resetSession \(String(describing: url)) ***")
        }
    }
    
    private func cleanupSession(player:AVPlayer?){
        objc_sync_enter(self)
        defer { objc_sync_exit(self) }
        if let player = player{
            self.removePlayerObservors(player: player);
        }
        self.player = nil
        AVPlayerIntegrationWrapper.logDebugStatement("removeSession ***")
    }
    
    private func continueStoppedSession(){
        guard let player = self.player else{
            AVPlayerIntegrationWrapper.logDebugStatement("!!! Error - continueStoppedSession failed, player not avl")
            return
        }
        guard let playerItem = self.player?.currentItem else {
            AVPlayerIntegrationWrapper.logDebugStatement("!!! Error - continueStoppedSession failed, playerItem not avl")
            return
        }
        self.sessTerminated = false;
        //Lets save the player events that were sent earlier. The ones those were pushed earlier, will reappear after deep init of session.
        infoEventsIdxToSkip = infoEventIdx
        errorEventsIdxToSkip = errEventIdx
        initSession(player: player, playerItem: playerItem, deep: true)
        setPresentationInformationForContent()
    }
    
    private func reportStoppedState(){
        if(currentState != .IDLE && currentState != .STOPPED){
            if let player = player {
                let time = player.currentTime();
                if(time.timescale > 0){
                    mmSmartStreaming.reportPlaybackPosition(Int((time.value)/(Int64)(time.timescale)) * 1000)
                }
            }
            mmSmartStreaming.report(.STOPPED)
            currentState = .STOPPED
        }
    }
    
    private func reportUserInitiatedPlayback(){
        mmSmartStreaming.reportUserInitiatedPlayback()
    }
    
    private func reportPlayerSeekCompleted(seekEndPos:Int){
        mmSmartStreaming.reportPlayerSeekCompleted(seekEndPos)
    }
    
    private func processDuration(change: [NSKeyValueChangeKey : Any]?){
        guard player != nil else {
            return;
        }

        let newDuration: CMTime
        if let newDurationAsValue = change?[NSKeyValueChangeKey.newKey] as? NSValue {
            newDuration = newDurationAsValue.timeValue
        }
        else {
            newDuration = kCMTimeZero
        }
        
        presentationDuration = newDuration
        setPresentationInformationForContent()
    }
    
    private func processDurationFromPlayerItem() {
        guard player != nil else {
            return;
        }
        
        if let item = self.player?.currentItem {
            presentationDuration = item.duration
            setPresentationInformationForContent()
        }
    }
    
    private func setPresentationInformationForContent(){
        guard let contentDuration = presentationDuration else{
            return
        }

        if !presentationInfoSet{
            let presentationInfo = MMPresentationInfo();
            let hasValidDuration = contentDuration.isNumeric && contentDuration.value != 0
            if(hasValidDuration){
                let newDurationSeconds = hasValidDuration ? CMTimeGetSeconds(contentDuration) : 0.0
                AVPlayerIntegrationWrapper.logDebugStatement("Duration of content is \(String(describing: newDurationSeconds * 1000))")
                let duration  = newDurationSeconds * 1000
                presentationInfo.duration = Int(duration)
            }else {
                presentationInfo.duration = Int(-1)
                presentationInfo.isLive = true
                
            }
            mmSmartStreaming.setPresentationInformation(presentationInfo)
            presentationInfoSet = true
        }
    }
    
    private var presentationDuration:CMTime?
    
    private func processPlaybackRate(_ rate:Float){
        switch rate {
        case 0.0:
            AVPlayerIntegrationWrapper.logDebugStatement("LATENCY: - Playback rate 0")
            if currentState != .PAUSED{
                mmSmartStreaming.report(MMPlayerState.PAUSED);
                currentState = .PAUSED
            }

        default:
            AVPlayerIntegrationWrapper.logDebugStatement("LATENCY: - Playback rate \(rate)")
        }
    }
    
    private func playbackFailed(){
        AVPlayerIntegrationWrapper.logDebugStatement("--- playbackFailed ---")
        var currContent = "Content Not Set";
        if let contentURL = self.contentURL{
            currContent = contentURL;
        }
        mmSmartStreaming.reportError(String("Playback of \(currContent) Failed"), atPosition: getPlaybackPosition())
        cleanupCurrItem();
    }
    
    private func processCurrentItemChange(old oldItem:AVPlayerItem?, new newItem:AVPlayerItem?){
        guard let player = self.player else{
            return
        }
        AVPlayerIntegrationWrapper.logDebugStatement("--Process current item changed with the player--")
        // Player Item has changed (asset being played)
        if let oldItem = oldItem{
            resetSession(item: oldItem)
        }

        if let newItem = newItem{
            initSession(player: player, playerItem: newItem, deep: true)
        }
    }
    
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard context == &AVFoundationPlayerViewControllerKVOContext || context == &AVFoundationPlayerViewControllerKVOContextPlayer else {
            super.observeValue(forKeyPath:keyPath, of: object, change: change, context: context)
            return
        }
        
        if keyPath == AVPlayerItemPropertiesToObserve.Duration.rawValue {
            processDuration(change: change)
        }
        else if keyPath == AVPlayerItemPropertiesToObserve.ItemStatus.rawValue {
            let newStatus: AVPlayerItem.Status
            if let newStatusAsNumber = change?[NSKeyValueChangeKey.newKey] as? NSNumber {
                newStatus = AVPlayerItem.Status(rawValue: newStatusAsNumber.intValue)!
                if newStatus == .failed{
                    playbackFailed();
                }
            }
            else {
                newStatus = .unknown
            }
            
            switch newStatus {
            case .readyToPlay:
                processDurationFromPlayerItem()
            default:
                print("")
            }
        }
        else if keyPath == AVPlayerItemPropertiesToObserve.PlaybackBufferEmpty.rawValue {
            mmSmartStreaming.reportBufferingStarted()
        }
        else if keyPath == AVPlayerItemPropertiesToObserve.PlaybackLikelyToKeepUp.rawValue { //Change to handle the event: kCMTimebaseNotification_EffectiveRateChanged for buffering completion
            mmSmartStreaming.reportBufferingCompleted()
        }
        else if keyPath == AVPlayerItemPropertiesToObserve.PresentationSize.rawValue {
            if let presentationSz = change?[NSKeyValueChangeKey.newKey]{
                let pSz = presentationSz as! CGSize
                mmSmartStreaming.reportPresentationSize(withWidth: Int(pSz.width), andHeight: Int(pSz.height))
            }
        }
        else if keyPath ==  AVPlayerPropertiesToObserve.PlaybackRate.rawValue{
            processPlaybackRate((object! as AnyObject).rate);
        }
        else if keyPath == AVPlayerPropertiesToObserve.CurrentItem.rawValue{
            let newItem = change?[NSKeyValueChangeKey.newKey] as? AVPlayerItem
            let oldItem = change?[NSKeyValueChangeKey.oldKey] as? AVPlayerItem
            processCurrentItemChange(old:oldItem, new:newItem);
        }
    }
    
    private func addPlayerObservors(){
        guard let player = self.player else{
            return
        }
        
        player.addObserver(self, forKeyPath: AVPlayerPropertiesToObserve.PlaybackRate.rawValue, options: [.new, .initial], context: &AVFoundationPlayerViewControllerKVOContextPlayer)
        player.addObserver(self, forKeyPath: AVPlayerPropertiesToObserve.CurrentItem.rawValue, options: [.new, .old], context: &AVFoundationPlayerViewControllerKVOContextPlayer)
        
        StartSeekWatchdogAndPlaybackPositionTracker()
        playerObserved = true
    }
    
    private func removePlayerObservors(player:AVPlayer){
        if(playerObserved == true){
            if let timeObservor = self.timeObservor{
                player.removeTimeObserver(timeObservor)
            }
            self.timeObservor = nil
            player.removeObserver(self, forKeyPath: AVPlayerPropertiesToObserve.PlaybackRate.rawValue);
            player.removeObserver(self, forKeyPath: AVPlayerPropertiesToObserve.CurrentItem.rawValue);
            playerObserved = false;
        }
    }
    
    
    private func processEventsRegister(){
        guard let player = self.player else{
            return
        }
        
        guard let playerItem = player.currentItem else{
            return
        }
        
        guard let observerAddedPlayerItem = self.playerItemObserverAdded else {
            return
        }
        
        if (observerAddedPlayerItem == playerItem) {
            if let evtCount = playerItem.accessLog()?.events.count {
                AVPlayerIntegrationWrapper.logDebugStatement("Total events \(String(describing: evtCount))")
                if (self.infoEventIdx > evtCount) {
                    self.errEventIdx = 0
                    self.infoEventIdx = 0
                    self.infoEventsIdxToSkip = -1
                    self.errorEventsIdxToSkip = -1
                }
                for i in stride(from: self.infoEventIdx, to: evtCount, by: 1) {
                    let accessEvt = playerItem.accessLog()?.events[i]
                    
                    if(infoEventsIdxToSkip >= infoEventIdx){ //We are deliberately letting download rate be passed in replay of prev session. Because, it may not be set again on replay ...
                        if let indicatedBitrate = accessEvt?.indicatedBitrate{
                            AVPlayerIntegrationWrapper.logDebugStatement("Skipping Event, indicatedBitrate \(indicatedBitrate)")
                            if  indicatedBitrate > 0.0{
                                lastObservedBitrateOfContinuedSession = indicatedBitrate
                            }
                        }
                        
                        if let obsBitrate = accessEvt?.observedBitrate{
                            if obsBitrate.isNormal{
                                lastObservedDownlaodRateOfContinuedSession = Int(obsBitrate)
                            }
                        }
                        
                        AVPlayerIntegrationWrapper.logDebugStatement("Skipping Event, Pending \(infoEventsIdxToSkip - infoEventIdx)")
                        infoEventIdx += 1;
                        continue
                    }
                    
                    if lastObservedBitrateOfContinuedSession > 0 {
                        currentBitrate = lastObservedBitrateOfContinuedSession
                        AVPlayerIntegrationWrapper.logDebugStatement("ABRSWITCH: !isInitialBitrateReported \(currentBitrate) => \(currentBitrate)")
                        mmSmartStreaming.reportABRSwitch(fromBitrate: Int(currentBitrate), toBitrate: Int(currentBitrate))
                        isInitialBitrateReported = true
                        lastObservedBitrateOfContinuedSession = -1.0
                    }
                    
                    if lastObservedDownlaodRateOfContinuedSession > 0 {
                        mmSmartStreaming.reportDownloadRate(lastObservedDownlaodRateOfContinuedSession)
                        lastObservedDownlaodRateOfContinuedSession = -1
                    }
                    
                    if let obsBitrate = accessEvt?.observedBitrate{
                        if obsBitrate.isNormal{
                            mmSmartStreaming.reportDownloadRate(Int(obsBitrate))
                        }
                    }
                    
                    if let indicatedBitrate = accessEvt?.indicatedBitrate{
                        if indicatedBitrate > 0.0{
                            if currentBitrate == 0{
                                currentBitrate = indicatedBitrate
                            }
                            
                            if(isInitialBitrateReported == false){
                                isInitialBitrateReported = true
                                AVPlayerIntegrationWrapper.logDebugStatement("ABRSWITCH: !isInitialBitrateReported \(currentBitrate) => \(currentBitrate)")
                                mmSmartStreaming.reportABRSwitch(fromBitrate: Int(currentBitrate), toBitrate: Int(currentBitrate))
                            }else{
                                AVPlayerIntegrationWrapper.logDebugStatement("ABRSWITCH: \(currentBitrate) => \(indicatedBitrate)")
                                if currentBitrate != indicatedBitrate{
                                    mmSmartStreaming.reportABRSwitch(fromBitrate: Int(currentBitrate), toBitrate: Int(indicatedBitrate))
                                }
                            }
                            currentBitrate = indicatedBitrate
                        }
                    }
                    
                    if let droppedFrames = accessEvt?.numberOfDroppedVideoFrames{
                        if droppedFrames > frameLossCnt{
                            mmSmartStreaming.reportFrameLoss((droppedFrames-frameLossCnt))
                            frameLossCnt = droppedFrames
                        }
                    }
                    infoEventIdx += 1;
                }
            }
            
            if lastObservedBitrateOfContinuedSession > 0 {
                currentBitrate = lastObservedBitrateOfContinuedSession
                AVPlayerIntegrationWrapper.logDebugStatement("ABRSWITCH: !isInitialBitrateReported \(currentBitrate) => \(currentBitrate)")
                mmSmartStreaming.reportABRSwitch(fromBitrate: Int(currentBitrate), toBitrate: Int(currentBitrate))
                isInitialBitrateReported = true
                lastObservedBitrateOfContinuedSession = -1.0
            }
            
            if lastObservedDownlaodRateOfContinuedSession > 0 {
                mmSmartStreaming.reportDownloadRate(lastObservedDownlaodRateOfContinuedSession)
                lastObservedDownlaodRateOfContinuedSession = -1
            }
        }
    }
    
    private func newAccessLogEntryRecvd(notification noti:Notification){
        processEventsRegister();
    }
    
    private func processErrorEventsRegister(){
        guard let player = self.player else{
            return
        }
        
        guard let playerItem = player.currentItem else{
            return
        }
        
        guard let observerAddedPlayerItem = self.playerItemObserverAdded else {
            return
        }
        
        if (observerAddedPlayerItem == playerItem) {
            if let errEvtCount = playerItem.errorLog()?.events.count {
                AVPlayerIntegrationWrapper.logDebugStatement("Total Err events \(String(describing: errEvtCount))")
                var kvpsForErr:[String] = []
                if (self.errEventIdx > errEvtCount) {
                    self.errEventIdx = 0
                    self.infoEventIdx = 0
                    self.infoEventsIdxToSkip = -1
                    self.errorEventsIdxToSkip = -1
                }
                for i in stride(from: self.errEventIdx, to: errEvtCount, by: 1) {
                    
                    if(errorEventsIdxToSkip >= errEventIdx){ //We are deliberately letting download rate be
                        
                        AVPlayerIntegrationWrapper.logDebugStatement("Skipping ERR Event, Pending \(errorEventsIdxToSkip - errEventIdx)")
                        errEventIdx += 1;
                        continue
                    }
                    
                    let errEvt = playerItem.errorLog()?.events[i]
                    if let sessID = errEvt?.playbackSessionID{
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KPlaybackSessionID + "=" + sessID)
                    }
                    
                    if let time = errEvt?.date{
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KErrorInstantTime + "=" + time.description)
                    }
                    
                    if let uri = errEvt?.uri{
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KPlaybackUri + "=" + uri)
                    }
                    
                    if let statusCode = errEvt?.errorStatusCode{
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KErrorStatusCode + "=" + String(statusCode))
                    }
                    
                    if let serverAddr = errEvt?.serverAddress{
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KServerAddress + "=" + String(serverAddr))
                    }
                    
                    if let errDomain = errEvt?.errorDomain{
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KErrorDomain + "=" + errDomain)
                    }
                    
                    if let errComment = errEvt?.errorComment{
                        kvpsForErr.append(AVPlayerIntegrationWrapper.KErrorComment + "=" + errComment)
                    }
                    self.errEventIdx += 1
                }
                
                if kvpsForErr.count > 0{
                    //Lets not distinguish errors, filterning can better be handled in backend
                    //Session termination can be handled via player notifications when it gives up
                    let errString  = kvpsForErr.joined(separator: ":")
                    mmSmartStreaming.reportError(errString, atPosition: getPlaybackPosition())
                }
            }
        }
    }
    
    private func newErrorLogEntryRecvd(notification noti:Notification){
        processErrorEventsRegister()
    }
    
    private static func isPossibleSeek(currpos:Int64, lastPos:Int64, currentRecordTime:Int64, lastRecInstant:Int64) -> Bool{
        if lastRecInstant < 0 || lastPos<0{
            return false
        }
        let posDiff = abs(currpos - lastPos)
        let wallclockTimeDiff = currentRecordTime - lastRecInstant
        let drift = Int(abs(wallclockTimeDiff - posDiff))
        AVPlayerIntegrationWrapper.logDebugStatement(" -- tick isPossibleSeek: playDelta [\(posDiff) msec] wallclkDelta [\(wallclockTimeDiff) msec] drift [\(drift) msec]");
        
        if drift > AVPlayerIntegrationWrapper.KPlaybackPosPollingIntervalMSec{
            return true
        }
        return false
    }
    
    private func StartSeekWatchdogAndPlaybackPositionTracker(){
        guard let player = self.player else {
            return
        }
        
        timeObservor = player.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(AVPlayerIntegrationWrapper.KPlaybackPosPollingIntervalSec, Int32(NSEC_PER_SEC)), queue: nil){ [weak self] time in
            
            
            guard let player = self?.player else{
                return
            }
            
            guard let playerItem = player.currentItem else{
                return
            }
            
            let currentPlaybackPos = Int64(CMTimeGetSeconds(time) * 1000);
            let currentRecordTime = Int64(CFAbsoluteTimeGetCurrent() * 1000)
            AVPlayerIntegrationWrapper.logDebugStatement(" -- tick Periodic Check -- pos=\(currentPlaybackPos) at=\(currentRecordTime)")
            if let timebase = playerItem.timebase{
                let observedRate = CMTimebaseGetRate(timebase)
                if observedRate > 0{
                    AVPlayerIntegrationWrapper.logDebugStatement(" -- tick observed rate \(observedRate) player rate \(player.rate) --")
                    if player.rate != 0 && currentPlaybackPos > 100{ //Atleast some playback is there
                        
                        if(self?.currentState == .PAUSED || self?.currentState == .IDLE){
                            AVPlayerIntegrationWrapper.logDebugStatement(" -- tick LATENCY: Report PLAYING --")
                            self?.timeoutOccurred()
                            self?.mmSmartStreaming.report(MMPlayerState.PLAYING);
                            self?.currentState = .PLAYING
                            if(self?.sessionInStartedState == false){
                                self?.sessionInStartedState = true;
                                self?.lastPlaybackPos = currentPlaybackPos
                                self?.lastPlaybackPosRecordTime = currentRecordTime
                            }
                        }
                    }
                    
                    if (self?.sessTerminated == true){
                        AVPlayerIntegrationWrapper.logDebugStatement(" -- tick Forcing restart of session via Play .., Replay Session]")
                        self?.continueStoppedSession()
                        if( self?.loadEventTriggered == false){
                            self?.mmSmartStreaming.reportUserInitiatedPlayback()
                            self?.loadEventTriggered = true
                        }
                    }
                }
                
                guard let lastPlaybackPos = self?.lastPlaybackPos else{
                    return;
                }
                
                guard let lastPlaybackPosRecordTime = self?.lastPlaybackPosRecordTime else{
                    return;
                }
                
                if(self?.sessionInStartedState == true){
                    if ((player.rate  == 0  && abs(lastPlaybackPos - currentPlaybackPos) > AVPlayerIntegrationWrapper.KMinPlaybackPosDriftForPausedStateMSec) || abs(lastPlaybackPos - currentPlaybackPos) > AVPlayerIntegrationWrapper.KMinPlaybackPosDriftForPlayingStateMSec){ // We are not in buffering
                        //Check for possibility of occurrence of seek
                        AVPlayerIntegrationWrapper.logDebugStatement(" -- tick process possibility of occurrence of seek \(currentRecordTime) - \(lastPlaybackPosRecordTime) = \(currentRecordTime - lastPlaybackPosRecordTime)")
                        
                        if (AVPlayerIntegrationWrapper.isPossibleSeek(currpos: currentPlaybackPos, lastPos: lastPlaybackPos, currentRecordTime: currentRecordTime, lastRecInstant: lastPlaybackPosRecordTime)){
                            if (self?.sessTerminated == true){
                                AVPlayerIntegrationWrapper.logDebugStatement(" -- tick Seek: Forcing restart of session via Seek ...")
                                self?.continueStoppedSession()
                            }
                            self?.reportPlayerSeekCompleted(seekEndPos: Int(currentPlaybackPos))
                        }
                    }else{
                        AVPlayerIntegrationWrapper.logDebugStatement(" -- tick process possibility of occurrence of seek playhead drift \(abs(lastPlaybackPos - currentPlaybackPos))")
                    }
                }
                
                self?.lastPlaybackPos = currentPlaybackPos
                self?.lastPlaybackPosRecordTime = currentRecordTime
            }
        }
    }
    
    //FREEWHEEL ADS FUNCTIONALITIES
    @objc public static func setFreeWheelContext(_adContext: FWContext) {
        AVPlayerIntegrationWrapper.shared.adContext = _adContext
        
        var observor = NotificationCenter.default.addObserver(forName: NSNotification.Name.FWAdEvent, object: _adContext, queue: nil, using: {  (not) in  AVPlayerIntegrationWrapper.shared.onFreeWheelAdEventNotified(notification:not)})
        
        AVPlayerIntegrationWrapper.shared.notificationObservors.append(observor)
        
        
        observor = NotificationCenter.default.addObserver(forName: NSNotification.Name.FWRequestComplete, object: _adContext, queue: nil, using: {  (not) in  AVPlayerIntegrationWrapper.shared.onFWRequestCompleted(notification:not)})
        AVPlayerIntegrationWrapper.shared.notificationObservors.append(observor)
    }
    
    private func onFWRequestCompleted(notification: Notification) {
        self.mmSmartStreaming.report(MMAdState.AD_REQUEST)
    }
    
    @objc private func checkFWAdTime() {
        if let adInstance = self.adInstance {
            if self.currentAdState != MMAdState.AD_CLICKED && self.currentAdState != MMAdState.AD_COMPLETED && self.currentAdState != MMAdState.AD_PAUSED && self.currentAdState != MMAdState.AD_SKIPPED && self.currentAdState != MMAdState.AD_ERROR && self.currentAdState != MMAdState.AD_ENDED {
                self.countFWAdTimer += 1
                if (self.countFWAdTimer % 5 == 0) {
                    self.mmSmartStreaming.reportAdPlaybackTime(self.countFWAdTimer * 1000)
                }
                if (Int(adInstance.playheadTime()) % 5 == 0) {
                    self.mmSmartStreaming.reportAdPlaybackTime(Int(adInstance.playheadTime()) * 1000)
                }
            }
        }
    }
    
    private func onFreeWheelAdEventNotified(notification: Notification) {
        if let userInfo = notification.userInfo {
            if let adInstance = userInfo["adInstance"] as? FWAdInstance {
                self.adInstance = adInstance
                self.fetchAndReportAdInfo(forAdInstance: adInstance)
            }
            
            if let subEventName = userInfo["subEventName"] as? String {
                switch subEventName {
                case FWAdImpressionEvent:
                    self.isAdBuffering = false
                    self.isAdStreaming = true
                    self.countFWAdTimer = 0
                    self.mmSmartStreaming.reportAdPlaybackTime(0)
                    self.mmSmartStreaming.report(MMAdState.AD_IMPRESSION)
                    self.currentAdState = MMAdState.AD_IMPRESSION
                    
                    self.isAdBuffering = false
                    self.isAdStreaming = true
                    self.countFWAdTimer = 0
                    self.mmSmartStreaming.reportAdPlaybackTime(0)
                    if (isAdBuffering) {
                        self.isAdBuffering = false
                    }
                    self.mmSmartStreaming.report(MMAdState.AD_STARTED)
                    self.currentAdState = MMAdState.AD_STARTED
                    
                    if let adInstance = userInfo["adInstance"] as? FWAdInstance {
                        if let creativeRendition = adInstance.primaryCreativeRendition() {
                            if creativeRendition.baseUnit() == "video" {
                                self.countFWAdTimer = 0
                                self.timerFWAd = Timer.scheduledTimer(timeInterval: 1.0, target:self, selector:#selector(AVPlayerIntegrationWrapper.checkFWAdTime), userInfo: nil, repeats: true)
                            }
                        }
                    }
                    
//                case FWAdLoadedEvent:
//                    self.isAdBuffering = false
//                    self.isAdStreaming = true
//                    self.countFWAdTimer = 0
//                    self.mmSmartStreaming.reportAdPlaybackTime(0)
//                    self.mmSmartStreaming.report(MMAdState.AD_IMPRESSION)
//                    self.currentAdState = MMAdState.AD_IMPRESSION
                    
                case FWAdClickEvent:
                    self.isAdStreaming = false
                    self.mmSmartStreaming.report(MMAdState.AD_CLICKED)
                    self.currentAdState = MMAdState.AD_CLICKED
                    
                case FWAdCompleteEvent:
                    self.isAdStreaming = false
                    self.mmSmartStreaming.reportAdPlaybackTime(self.adDuration * 1000)
                    self.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
                    self.currentAdState = MMAdState.AD_COMPLETED
                    self.timerFWAd?.invalidate()
                    if (self.isPostRollAd) {
                        self.cleanupCurrItem();
                    }
                    
                case FWAdCloseEvent:
                    self.isAdStreaming = false
                    self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
                    self.mmSmartStreaming.report(MMAdState.AD_COMPLETED)
                    self.currentAdState = MMAdState.AD_ENDED
                    self.timerFWAd?.invalidate()
                    if (self.isPostRollAd) {
                        self.cleanupCurrItem();
                    }
                    
                    
                case FWAdPauseEvent:
                    self.isAdStreaming = true
                    self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
                    self.mmSmartStreaming.report(MMAdState.AD_PAUSED)
                    self.currentAdState = MMAdState.AD_PAUSED
                    
                case FWAdResumeEvent:
                    self.isAdStreaming = true
                    self.mmSmartStreaming.report(MMAdState.AD_RESUMED)
                    self.currentAdState = MMAdState.AD_RESUMED
                    
                case FWAdBufferingStartEvent:
                    self.mmSmartStreaming.reportAdBufferingStarted()
                    
                case FWAdBufferingEndEvent:
                    self.mmSmartStreaming.reportAdBufferingCompleted()
                    
                case FWAdSkippedByUserEvent:
                    self.isAdStreaming = false
                    self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
                    self.mmSmartStreaming.report(MMAdState.AD_SKIPPED)
                    self.currentAdState = MMAdState.AD_SKIPPED
                    
                case FWAdFirstQuartileEvent:
                    self.isAdStreaming = true
                    self.mmSmartStreaming.report(MMAdState.AD_FIRST_QUARTILE)
                    self.currentAdState = MMAdState.AD_FIRST_QUARTILE
                    
                case FWAdMidpointEvent:
                    self.isAdStreaming = true
                    self.mmSmartStreaming.report(MMAdState.AD_MIDPOINT)
                    self.currentAdState = MMAdState.AD_MIDPOINT
                    
                case FWAdThirdQuartileEvent:
                    self.isAdStreaming = true
                    self.mmSmartStreaming.report(MMAdState.AD_THIRD_QUARTILE)
                    self.currentAdState = MMAdState.AD_THIRD_QUARTILE
                    
                case FWAdErrorEvent:
                    self.mmSmartStreaming.reportAdPlaybackTime(Int(self.adInstance!.playheadTime()) * 1000)
                    self.mmSmartStreaming.report(MMAdState.AD_ERROR)
                    self.currentAdState = MMAdState.AD_ERROR
                
                default:
                    print("Other events")
                }
            }
            if let _ = userInfo["slot"] {
                //print(adSlot)
            }
        }
    }
    
    private func fetchAndReportAdInfo(forAdInstance adInstance: FWAdInstance!) {
        guard let playerItem = self.player?.currentItem else {
            return
        }
        let adInfo = MMAdInfo()
        adInfo.adClient = "freewheel"
        adInfo.adId = String(adInstance.adId())
        if let creativeRendition = adInstance.primaryCreativeRendition() {
            if creativeRendition.width() > 0 {
                adInfo.adResolution = "\(creativeRendition.width())x\(creativeRendition.height())"
            }
            self.adDuration = Int(creativeRendition.duration())
            adInfo.adDuration = Int(creativeRendition.duration() * 1000)
            adInfo.adCreativeType = creativeRendition.contentType()
        }
        
        if (self.getPlaybackPosition() == 0) {
            adInfo.adPosition = "pre"
            self.isPostRollAd = false
            adInfo.adPodIndex = 0
        } else if (!CMTimeGetSeconds(playerItem.duration).isNaN && self.getPlaybackPosition() == Int(CMTimeGetSeconds(playerItem.duration)) * 1000) {
            adInfo.adPosition = "post"
            self.isPostRollAd = true
            adInfo.adPodIndex = -1
        } else {
            adInfo.adPosition = "mid"
            self.isPostRollAd = false
            adInfo.adPodIndex = 1
        }
        adInfo.adPodLendth = 1
        adInfo.adPositionInPod = 1
        
        adInfo.isBumper = false
        adInfo.adType = MMAdType.AD_LINEAR
        //adInfo.adServer = advertisement.adSystem
        self.mmSmartStreaming.report(adInfo)
    }
    
    private enum CurrentPlayerState{
        case IDLE,
        PLAYING,
        PAUSED,
        STOPPED,
        ERROR
    };
    
    private var sessTerminated:Bool = false
    private var timer:Timer?
    weak private var player:AVPlayer?
    weak private var playerItemObserverAdded: AVPlayerItem?
    
    weak private var adContext: FWContext?
    weak private var adInstance: FWAdInstance?
    private var timerFWAd: Timer?
    private var isAdStreaming = false
    private var isAdBuffering = false
    private var isPostRollAd = false
    private var countFWAdTimer = 0
    private var adDuration = 0
    private var currentAdState = MMAdState.UNKNOWN
    
    private let TIME_INCREMENT = 2.0
    private var presentationInfoSet = false
    private var infoEventIdx = 0
    private var errEventIdx = 0
    private var infoEventsIdxToSkip = -1
    private var errorEventsIdxToSkip = -1
    private var durationWatchedTotal:TimeInterval = 0
    private var currentBitrate:Double = 0
    private var frameLossCnt:Int = 0
    private var isInitialBitrateReported = false;
    private var lastObservedBitrateOfContinuedSession = -1.0
    private var lastObservedDownlaodRateOfContinuedSession = -1
    private var contentURL:String?
    private var currentState = CurrentPlayerState.IDLE
    private var lastPlaybackPos:Int64 = 0
    private var lastPlaybackPosRecordTime:Int64 = 0
    private var notificationObservors = [Any]()
    private var timeObservor:Any?
    private var playerObserved:Bool = false
    private var assetInfo:MMAVAssetInformation?
    private var sessionInStartedState = false
    private var loadEventTriggered = false
    private static var enableLogging = false
    
    private enum AVPlayerPropertiesToObserve:String{
        case PlaybackRate = "rate",
        CurrentItem = "currentItem"
    }
    
    private enum AVPlayerItemPropertiesToObserve: String{
        case Duration = "duration",
        Playable = "playable",
        ItemStatus = "status",
        PresentationSize = "presentationSize",
        PlaybackBufferEmpty = "playbackBufferEmpty",
        PlaybackLikelyToKeepUp = "playbackLikelyToKeepUp"
    }
    
    private lazy var playerEventsQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "Player Events Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    lazy var mmSmartStreaming:MMSmartStreaming = MMSmartStreaming.getInstance() as! MMSmartStreaming
    private static let KPlaybackSessionID = "PlaybackSessionID"
    private static let KErrorInstantTime = "ErrorInstantTime"
    private static let KPlaybackUri = "PlaybackUri"
    private static let KErrorStatusCode = "StatusCode"
    private static let KServerAddress = "ServerAddress"
    private static let KErrorDomain = "ErrorDomain"
    private static let KErrorComment = "ErrorComment"
    private static let KPlaybackPosPollingIntervalSec = 0.5
    private static let KPlaybackPosPollingIntervalMSec = 500
    private static let KMinPlaybackPosDriftForPlayingStateMSec = 400
    private static let KMinPlaybackPosDriftForPausedStateMSec = 200
    private static var appNotificationObsRegistered = false;
}

extension Date {
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        return end - start
    }
}
